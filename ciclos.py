'''--------------CICLOS--------------'''
incremental = 0
'''
while incremental <= 100:
    print(incremental)
    #incremental = incremental + 1
    incremental += 1


print("Fin del while")

num_1 = 10
num_2 = 5

num_1 += num_2
num_1 *= num_2
num_2 /= num_2
'''

'''
Crear menú con while
'''

'''
    ------------------------------------------------
    1 -> Suma
        Ingrese el primer número >> 
        Ingrese el segundo número >> 
        {nummero_1} + {numero_2} = {resultado}
    2 -> Resta
        Ingrese el primer número >> 
        Ingrese el segundo número >> 
        {nummero_1} - {numero_2} = {resultado}
    3 -> Salir
    ------------------------------------------------
'''
opc = 0
while opc != 3:
    mensaje_menu = "------------------------------------------------\n"
    mensaje_menu += '1 -> Suma \n'
    mensaje_menu += '2 -> Resta \n'
    mensaje_menu += '3 -> Salir\n'
    mensaje_menu += '>>> '

    #Capturar opción ingresada por el usuario
    opc = int( input(mensaje_menu) )

    if opc == 1:
        try:
            num_1 = float( input('Ingrese el primer número >> ') )
            num_2 = float( input('Ingrese el segundo número >> ') )
            suma = num_1 + num_2
            #Formateando la salida de datos
            print(f"{num_1} + {num_2} = {suma}")
        except:
            print('Datos inválidos')
    elif opc == 2:
        try:
            num_1 = float( input('Ingrese el primer número >> ') )
            num_2 = float( input('Ingrese el segundo número >> ') )
            resta = num_1 - num_2
            #Formateando la salida de datos
            print(f"{num_1} - {num_2} = {resta}")
        except:
            print('Datos inválidos')
    elif opc != 3:
        print('Ingrese una opción válida')






