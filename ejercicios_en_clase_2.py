#Mostrar en pantalla
'''
print('Hola usuario!\nIngrese el nivel de dificultad:')
try:
    #Solicitando datos
    nivel = int(input('1 -> Fácil \n2-> Medio \n3->Avanzado\n>>>'))
    print('Ejercicios a realizar: ')
    #Condicionales
    if nivel == 1:
        print('-> 10 flexiones de pecho \n->10 sentadillas')
    elif nivel == 2:
        print('-> 20 burpes \n-> 50 flexiones de pecho \n-> 50 sentadillas')
    elif nivel == 3:
        print('-> 100 polichilenos \n-> 100 flexiones de pecho con palmada \n-> 100 sentadillas')
    else:
        print('Por favor ingrese ina opción válida')
except:
    print('Ingrese un valor numérico')
'''

'''
\n -> Salto de linea
\t -> Tabulador
\" -> Mostrar comillas
'''
mensaje_salto_de_linea = "Hola mundo \n este es mi primer programa"
print(mensaje_salto_de_linea)

tabulador = "Esto es un \t tabulador"
print(tabulador)

comillas = "Esto es una comilla (\")"
print(comillas)