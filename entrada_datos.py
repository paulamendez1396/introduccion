'''
input toma el dato por defecto como un String
'''
'''
nombre = input('Por favor ingrese su nombre: ')
apellido = input('Por favor ingrese su apellido: ')
#Concatenar
nombre_completo = 'Nombre: '+nombre+'\nApellido: '+apellido
print(nombre_completo)

print('Su nombre es %s ' %nombre)
print('Su nombre es ', nombre)
'''
#Castear un dato / convertir un dato
#Crear variables de tipo string
str_num_1 = "5"
str_num_2 = "5"
#Concatena variables con '+'
suma = str_num_1 + str_num_2
print(suma)
print("-------------------------")
#Castear datos/convertir a numérico (entero)
int_num_1 = int(str_num_1)
int_num_2 = int(str_num_2)
#Realiza operación de suma entre dos enteros
suma = int_num_1 + int_num_2
print(suma)
#castear como flotante
pi = float("3.14159")

#------------------------------------
#manejo de excepciones
try:
    #aquí va el código que posiblemente pueda marcar error
    num_1 = float(input('Por favor ingrese un número: '))
    num_2 = float(input('Por favor ingrese un segundo número: '))
    suma = num_1 + num_2
    #Salida por consola utilizando concatenación de 'formato' con flotante (formateando la salida)
    #con %.1f -> .# indica la cantidad de decimales que se quieren mostrar
    print('La suma es %.1f' %suma)
except:
    #aquí va la excepción o manejo del error
    print("Debe ingresar únicamente valores numéricos")

