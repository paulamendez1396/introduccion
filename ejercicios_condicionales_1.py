
num_1 = 5
num_2 = 4
num_3 = 6

#Condicionales anidados
if num_1 == num_2:
    print('num_1 == num_2')
else:
    if(num_1 == num_3):
        print('num_1 == num_3')
    else:
        print('num_1 != num_2 and num_1 != num_3')

#Condicionales en cascada
if num_1 == num_2:
    print('num_1 == num_2')
elif num_1 == num_3:
    print('num_1 == num_3')
else:
    print('num_1 != num_2 and num_1 != num_3')


if num_1 != num_3 and num_3 != num_2:
    print('num_1 != num_3 and num_3 != num_2')
elif num_1 == num_3 and num_3 != num_2:
    print('num_1 == 3 and num_3 != num_2')
else:
    print('No cumple ninguna de las anteriores')
